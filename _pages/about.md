---
layout: about
title: about
permalink: /
subtitle: #<a href='#'>Affiliations</a>. Address. Contacts. Moto. Etc.

profile:
  align: right
  image: me.png
  image_cicular: false # crops the image to make it circular
  address: >

news: false  # includes a list of news items
selected_papers: true # includes a list of papers marked as "selected={true}"
social: true  # includes social icons at the bottom of the page
---

I graduated with my Ph.D. at the University of Rennes in July 2023.
My Ph.D. was between the ENEDIS company (the main French electricity distributor) and the research lab IRISA since 2020.
My Ph.D. is aimed at studying the privacy risks related to the open data publication of electricity consumption.

More on that [here](/cv).


Email : antonin [at] voyez [dot] eu<br>
PGP key: [BD34 F6EB A1DD 816A 5E37 113C 6157 7F9E B6DB CDF7](/assets//key/Antonin Voyez_antonin@voyez.eu-0x61577F9EB6DBCDF7-pub.asc)