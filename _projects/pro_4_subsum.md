---
layout: page
title: SubSum implementation
description:
    Implementation of the SubSum attack. (see "Membership Inference Attacks on Aggregated Time Series with Linear Programming", 2022)
    <br>
    <br>
    (Python, Gurobi, Pandas, Seaborn/MPL)
redirect: https://gitlab.com/phd_antonin/subsum
img:
importance: 2
category: work
---