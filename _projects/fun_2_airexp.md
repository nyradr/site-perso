---
layout: page
title: AirExp
description:
    AirExp is a prettier interface to explore the data from the OurAirports database.
    <br>
    <br>
    (JS, Python, Vue, Leaflet, Flask, SQLite, Docker)
redirect: https://airexp.voyez.eu/
img: /assets/img/airexp.jpg
importance: 1
category: fun
---