---
layout: page
title: STATS attack implementation
description:
    Implementation of the STATS attack.
    <br>
    <br>
    (Python, Pandas, Ray, sklearn/sktime, Seaborn/MPL)
redirect: https://gitlab.com/phd_antonin/mia-ts
img:
importance: 2
category: work
---