---
layout: page
title: OgmAPI
description:
    Merge, query and display multiple geographical datasets from the French open data [data.gouv.fr](https://www.data.gouv.fr/) into a single unified database.
    <br>
    <br>
    (Python, Django, Airflow, JS, Vue, Leaflet)
redirect: https://ogmapi.fr
img: /assets/img/iedb.png
importance: 0
category: fun
---
