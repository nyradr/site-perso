---
layout: page
title: TS Unicity implementation
description:
    Compute uniqueness and k-anonymity set of time series. (see "Unique in the Smart Grid -The Privacy Cost of Fine-Grained Electrical Consumption Data", 2022)
    <br>
    <br>
    (Python, Pandas, Seaborn/MPL)
redirect: https://gitlab.com/phd_antonin/tsunicity
img:
importance: 2
category: work
---