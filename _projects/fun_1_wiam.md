---
layout: page
title: WIAM
description:
    WIAM (What Is Around Me) is an openstreetmap interface to explore map elements.
    <br>
    <br>
    (JS, Vue, Leaflet, OpenStreetMap, Overpass turbo API)
redirect: https://wiam.voyez.eu/
img: /assets/img/wiam.png
importance: 1
category: fun
---
