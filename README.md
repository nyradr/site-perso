# voyez.eu

This site is currently deployed at [https://voyez.eu](https://voyez.eu) and is based on a customized [al-folio](https://github.com/alshedivat/al-folio) template.

On top of the al-folio features, this site features a CV generation system and a [polyglot](https://polyglot.untra.io/) integration (for French and English).

## How to edit

All the content is either defined in the `_config.yml` or in the `_data/:lang/*.yml` files. Edit the content of those files to change the content of the website.

## How to use

See [jekyll](https://jekyllrb.com/) for the jekyll setup.

- Build the dependencies
```
bundler install
```

- Launch the dev server
```
bundle exec jekyll serve
```

- Deploy the production version:
```
bundle exec jekyll build
```