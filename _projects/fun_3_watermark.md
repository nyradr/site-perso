---
layout: page
title: watermark
description: 
    A web tool to watermark PDF documents.
    <br>
    <br>
    (JS, React, Gitlab pages, PDF-lib)
redirect: https://nyradr.gitlab.io/watermark/
img: /assets/img/watermark.png
importance: 1
category: fun
---