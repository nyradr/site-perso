---
layout: post
title: Large scale reproducible experiments (LACODAM Tahiti seminar)
date: 2023-06-09
description: Presentation's slides for the LACODAM Tahiti seminar.
tags: slides experiments
categories: slides
---
The slides can be found [here](/assets/pdf/pres_2023_06_09_reproducible.pdf).

<object data="{{ site.url }}{{ site.baseurl }}/assets/pdf/pres_2023_06_09_reproducible.pdf" width="1000" height="1000" type="application/pdf"></object>