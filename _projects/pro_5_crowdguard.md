---
layout: page
title: crowdpriv demo
description: 
    Demonstration of the crowdguard project.
    (see "Task-Tuning in Privacy-Preserving Crowdsourcing Platforms", 2020)
    <br>
    <br>
    (Python, Django, Vue, Bootstrap)
img: 
redirect: https://gitlab.inria.fr/crowdguard-public/implems/pkd-demo
importance: 3
category: work
---